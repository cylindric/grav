---
title: 'Day Sailor'
media_order: 'front.jpg,side.jpg'
taxonomy:
    category:
        - Woodworking
    tag:
        - toys
        - boats
        - sailing
---

This is the first toy boat I've built, the from the "Scroll Saw Magic Floating Boats" set from [ToyMakingPlans](https://www.toymakingplans.com/website/PlanSets/scrollsawmagic-floating-boats.html).

I've bought a few of their plans now, and also downloaded the free ones they have available, and they are of
incredible quality. My first impression from the website was that it was a little dated, and experience led
me to expect some fairly crude, or at least basic, PDF plans. This was far from the truth. The plans are amazing.

===

The plans include:

* many photographs of the finished items, some painted some natural
* overview plans showing all the components
* full-size diagrams that can be simply printed and stuck to the stock material
* box art for those looking to gift or sell the finished product

===

![front](front.jpg?cropResize=800,800)

![side](side.jpg?cropResize=800,800)
