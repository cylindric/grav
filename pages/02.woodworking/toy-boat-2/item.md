---
title: 'Torpedo Speeder'
media_order: 'hull.jpg,ballasting.jpg,dryingout.jpg'
taxonomy:
    category:
        - Woodworking
    tag:
        - toys
        - boats
---

I'm currently working on the second toy boat, the Torpedo Speeder, from the "Scroll Saw Magic Floating Boats" set from [ToyMakingPlans](https://www.toymakingplans.com/website/PlanSets/scrollsawmagic-floating-boats.html).

===

![wip day 2](hull.jpg?cropResize=800,800)

This is after "day 2", at which point the keel, the sides and the main deck were pretty much done.

![wip day 3 ballasing](ballasting.jpg?cropResize=800,800) ![wip day 3 drying out](dryingout.jpg?cropResize=800,800)

Here we are after day 3. I finished making all the extra bits that adorn the deck, such as the engine cover, exhaust pipes, seats and a few other bits. It was time to find out how much ballast would be required to make the boat sit at the correct depth in the water. Because this was done before any finish was applied, in case I had to make some alterations to allow for additional ballast, I then left the boat in a bed of rice to fully dry out. As we were in the midsts of an epic heat wave, that was probably pointless.
