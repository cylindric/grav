---
title: 'Lathe Tools'
media_order: woodworking-lathe-chisels.jpg
taxonomy:
    category:
        - Tools
    tag:
        - lathe
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
page-toc:
  active: true
---

These are the various chisels, gouges and other tools I've bought for the lathe.

===

# Warco Set of 8 Woodworking Lathe Chisels

I bought this set of 8 woodturning lathe chisels [from Warco](https://www.warco.co.uk/woodworking-chisels-wood-lathes/302842-woodworking-lathe-chisels.html) at the same time as the lathe.

Superb build quality throughout - these woodworking tools are built to last.

![Warco Lathe Chisels](woodworking-lathe-chisels.jpg?cropResize=300)

* High speed steel.
* Hardened and ground.
* Hardwood handles.
* Supplied complete with wooden storage case.

Includes the following chisels:

* 1" continental spindle gouge
* 3/4" continental spindle gouge
* 3/8" spindle gouge
* 3/4" round nosed scraper
* 90° sheer scraper
* 1/4" parting off tool
* 1/2" skew chisel
* 1" skew chisel
