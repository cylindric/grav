---
title: 'Mini Lathe'
media_order: 'mini-woodworking-woodturning-lathe.jpg,mini-wood-lathe-chuck.jpg'
taxonomy:
    category:
        - Tools
    tag:
        - lathe
page-toc:
  active: true
  start: 1
  end: 1
---

This the lathe, and various accessories for it.

===

# Warco Mini Wood Lathe

I bought this lovely little lathe from a local machine tool supplier, [Warco](https://www.warco.co.uk/wood-lathes/302917-mini-woodworking-woodturning-lathe.html)

![Warco Mini Wood Lath](mini-woodworking-woodturning-lathe.jpg?cropResize=300)

## Specifications (Copied from the website)

Quality variable speed mini lathe, ideal for a range of precise woodwork projects.

A compact sized hobby woodworker, capable of very intricate wood turning.

Supplied with a revolving and drive centre, machine is ready to go.

Features include

* Digital rev. counter
* Tool rest
* Ground bed ways
* Quick acting camlock tailstock
* Rigid cast iron construction
* Powerful 550w motor
* Included with every lathe for no extra charge:
  * Faceplate - 80mm
  * Drive centre - 2MT
  * Four prong drive centre - 2MT
* Specification
  * Centre height: 125mm
  * Distance between centres: 440mm
  * Spindle speeds: 750 / 3200 rpm
  * Face plate: 80mm
  * Diameter of tool rest support: 16mm
  * Spindle thread: 1" x 8 TPI
  * Headstock: 2MT
  * Tailstock: 2MT
  * Motor: 550w
  * Overall dimensions (L x W x H): 820 x 200 x 380mm
  * Weight: 38kg

# Mini Wood Lathe 4-Jaw Chuck

Another purchase from [Warco](https://www.warco.co.uk/woodturning-lathe-chucks-arbors/302914-mini-wood-lathe-chuck.html) was their Mini Wood Lathe 4-jaw chuck.

![Warco Mini Wood Lathe 4-jaw Chuck](mini-wood-lathe-chuck.jpg?cropResize=300)

Specification:

* Diameter - 100mm
* Thread - 1" x 8 TPI
* Index plate on rear of chuck
* Supplied with dovetail jaw set and threaded centre which is gripped in chuck base jaws

Standard jaw capacity:

* Gripping capacity: 32mm - 46mm
* Expanding capacity: 59 mm - 72mm
