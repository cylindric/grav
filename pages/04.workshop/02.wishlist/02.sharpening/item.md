---
title: 'Sharpening'
media_order: 'scheppach_tiger.jpg'
taxonomy:
    category:
        - Tools
---

Scheppach Tiger 2000S Wet Grinder

£121.95 : [Amazon](https://www.amazon.co.uk/dp/B00DOYWJVW)

===

![Scheppach Tiger 2000S Wet Grinder](scheppach_tiger.jpg?cropResize=300)

TiGer 2000s Sharpening and Honing System Professional Sharpening & Honing Results Made Simple - Even For The Amateur The perfect machine to eliminate frustration and costly errors because of blunt or incorrectly sharpened tools. The TiGer 2000s sharpening system cab offer superb, professional results that can only come from tools with razor sharp edges. The ideal machine offers all you need to start sharpening tools such as knives, plane irons and chisels. Features: Sturdy plastic body Grinding stone is suitable for HSS tools Full range of jigs and accessories available K220 Grit Stone for super fine sharpening.

Box Contains:

* 2 Jigs
* Angle Guide
* Honing Compound
