---
title: 'Wishlist'
media_order: 'scheppach_tiger.jpg,axminster_awede2.jpg'
content:
    items: '@self.children'
taxonomy:
    category:
        - Tools
---

This is just a few of the tools and equipment that I think I'd like for my workshop...

===

Some other equipment that would be great to have:

* Compressed Air
